//
//  Array.h
//  CommandLineTool
//
//  Created by Cameron MacPhail on 09/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__Array__
#define __CommandLineTool__Array__

#include <iostream>
template <class Type>
class Array
{
public:
    Array()/** Constructor */
    {
        data = nullptr;
        arraySize = 0;

    }
    ~Array()/** Destructor */
    {
        if (data != nullptr)
        {
            delete[] data;
        }

    }
    void add (float itemValue)
    {
        int i;
        Type* tempData = new Type[arraySize + 1];
        for(i=0;i < arraySize; i++)
        {
            tempData[i] = data[i];
        }
        
        tempData[arraySize] = itemValue;
        
        if (data != nullptr)
        {
            delete[] data;
        }
        arraySize++;
        data = tempData;
    }
    float get(int index)
    {
        return data[index];
    }
    int size()
    {
        return arraySize;
    }
    bool operator==(const Array&  otherArray)
    {
        if(size() != otherArray.size())
        {
            return false;
        }
        else
        {
            for(int i=0;i<arraySize;i++)
            {
                if(get(i) == otherArray.get(i))
                {
                
                }
                else
                {
                    return false;
                }
            }
            
            return true;
        }
    }

private:
    Type* data;
    int arraySize;
    
};



#endif /* defined(__CommandLineTool__Array__) */
