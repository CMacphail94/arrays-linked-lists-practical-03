//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"

void testArray()
{
    int size;
    float atIndex;
    Array<int> testArray;
    Array<int> testArray1;
   
    
    testArray.add(1);
    
    testArray.add(2);
    
    testArray.add(3);
    
    testArray.add(4);
    testArray1.add(1);
    
    testArray1.add(2);
    
    testArray1.add(3);
    
    testArray1.add(4);
    
    size = testArray.size();
    atIndex = testArray.get(1);
    
    std::cout <<"Array Length:"<<size;
    std::cout <<"\n 2: "<<atIndex;
    
      bool result = testArray==testArray1;
    
    std::cout << "\n" << result;
    
}

int main (int argc, const char* argv[])
{

    testArray();
  
    
}

